import { IMatchInterface } from "./IMatchInterface";

export interface IMatchDetailProps {
    selectedMatch: IMatchInterface;
    closeMatch: any;
    parentHandler: any;
}