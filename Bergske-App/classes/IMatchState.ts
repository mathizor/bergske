import { IMatchInterface } from "./IMatchInterface";

export interface IMatchState {
    loading: boolean;
    matchArray: any[];
    matches: IMatchInterface[];
    selectedMatch: IMatchInterface;
}