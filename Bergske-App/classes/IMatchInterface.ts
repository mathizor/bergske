export interface IMatchInterface {
    datum: Date;
    fK_Tegenstander_Id: number;
    doelpuntenVoor: number;
    doelpuntenTegen: number;
    tegenstanderNaam: string;
    id: number;
    opponent: any;
    thuis: boolean;
}
export interface IMatchInterfaceGrouped {
    [key: string]: IMatchInterface[];
}