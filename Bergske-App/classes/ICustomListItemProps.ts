import { IMatchInterface } from "./IMatchInterface";

export interface ICustomListItemProps {
    match: IMatchInterface;
    handler: any;
}