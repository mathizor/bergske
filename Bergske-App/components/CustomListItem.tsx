import React, { Component } from 'react';
import { ICustomListItemProps } from '../classes/ICustomListItemProps';
import { Text, View } from 'react-native';
import { ListItem } from 'react-native-elements'
import moment from "moment";

export default class CustomListItem extends React.Component<ICustomListItemProps, {}> {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <ListItem 
        title={<View style={{flexDirection: 'row'}}>
          <Text>{this.props.match.tegenstanderNaam} </Text>
          {this.getScore()}
        </View>}
        leftElement={
          <View>
            <Text style={{width: 40, textAlign: "center"}}>{this.getDayOfMonth(this.props.match.datum)}</Text>
            <Text style={{width: 40, textAlign: "center"}}>{this.getStartingHourAndMinutesFromMatch(this.props.match.datum)}</Text>
          </View>
        }
        rightElement= {
          <Text style={{width: 50}}>
            {this.props.match.thuis ? "Thuis" : "Uit"}
          </Text>
        }
        onPress={
            () => this.executeParentProps()
        }
      />
    );
  }
  private getStartingHourAndMinutesFromMatch(datum: Date) : string {
    var date: Date = new Date(datum.toString());
    moment.locale('nl');
    return moment(date).utc().format("HH:mm");
  }
  private getDayOfMonth(date: Date): number {
    var datum = new Date(date.toString());
    return datum.getDate();
  }
  private executeParentProps() : void {
    this.props.handler(this.props.match);
  }
  private getScore() : JSX.Element {
    var fontColor = "black";
    var matchText = "";

    if (this.matchInPast() && this.props.match.doelpuntenTegen != null && this.props.match.doelpuntenVoor != null) {
      if (this.props.match.thuis) {
        matchText = this.props.match.doelpuntenVoor + " - " + this.props.match.doelpuntenTegen;
      } else {
        matchText = this.props.match.doelpuntenTegen + " - " + this.props.match.doelpuntenVoor;
      }
      fontColor = this.getFontColor();
    }
    

    return <Text style={{color: fontColor, position: "absolute", right: 0}}>
      {matchText}
    </Text>;
  }
  private matchInPast() : boolean {
    var matchDate = new Date(this.props.match.datum.toString());
    return matchDate < new Date() ? true : false;
  }
  private getFontColor() : string {
    if (this.props.match.doelpuntenVoor == this.props.match.doelpuntenTegen) {
      return "yellow";
    } else if (this.props.match.doelpuntenVoor > this.props.match.doelpuntenTegen){
      return "green";
    } else {
      return "red";
    }
  }
}