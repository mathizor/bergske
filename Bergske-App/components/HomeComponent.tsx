import React, { Component } from 'react';
import {
  View,
  Platform,
  Button,
} from 'react-native';
import { Header } from 'react-native-elements';
import { IHomeState } from '../classes/IHomeState';
import MatchesView from './MatchesComponent';
export default class HomeView extends React.Component<{}, IHomeState> {
  constructor(props) {
    super(props);
    this.handler = this.handler.bind(this);
    this.state = {
      showMatches: false,
    };
  }
  private handler() {
    this.setState({
      showMatches: false
    })
  }

  render() {
    return (
      <View>
        {   
          this.state.showMatches 
            ? <MatchesView handler={this.handler}/>
            : <View>
              <Header
                centerComponent={{ text: "FC 't Bergske", style: { color: '#fff', fontSize: 20 }}}
                leftComponent={{ icon: 'home', color: '#fff', onPress: () => this.setState({
                    showMatches: false
                })}}
              />
              <Button onPress={() => {this.setState({showMatches: true})}} title={"Toon wedstrijden"}/>
            </View>
        }
      </View>
    );
  }
}