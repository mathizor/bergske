import React, { Component } from 'react';
import {
  View, Text, Button
} from 'react-native';
import {
  Header
} from 'react-native-elements';
import { IMatchDetailState } from '../classes/IMatchDetailState';
import { IMatchDetailProps } from '../classes/IMatchDetailProps';
import moment from "moment";
import MatchSpelersComponent from './MatchSpelersComponent';
export default class MatchDetailView extends React.Component<IMatchDetailProps, IMatchDetailState> {
  constructor(props) {
    super(props);
    this.state = {
        loading: true
    };
  }
  
  render() {
    return (
        <View>
            <Header
              centerComponent={{ text: this.props.selectedMatch.tegenstanderNaam, style: { color: '#fff', fontSize: 20 }}}
              leftComponent={{ icon: 'home', color: '#fff', onPress: () => this.closeParentState()}} 
              rightComponent={{icon: 'close',  color: '#fff', onPress: () => this.closeMatch() }}
            />
            <Text>Hier worden de details van de wedstrijd getoond...</Text>
            <Text>Aanvangsuur wedstrijd: {this.getStartingHourAndMinutesFromMatch(this.props.selectedMatch.datum)}</Text>
            <MatchSpelersComponent matchId={this.props.selectedMatch.id} />
        </View>
    );
  }

  private getStartingHourAndMinutesFromMatch(datum: Date) : string {
    var date: Date = new Date(datum.toString());
    moment.locale('nl');
    return moment(date).utc().format("HH:mm");
  }

  private closeParentState(): void {
    this.props.parentHandler();
  }
  private closeMatch() : void {
    this.props.closeMatch();
  }
}