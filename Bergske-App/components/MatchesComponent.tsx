import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Platform,
  Alert,
  SectionList,
  ActivityIndicator
} from 'react-native';
import { Header } from 'react-native-elements';
import { IMatchState } from '../classes/IMatchState';
import { IMatchProps } from '../classes/IMatchProps';
import { IMatchInterface } from '../classes/IMatchInterface';
import MatchDetailView from './MatchDetailScreen';
import CustomListItem from './CustomListItem';
export default class MatchesView extends React.Component<IMatchProps, IMatchState> {
  private monthNames = ["Januari", "Februari", "Maart", "April", "Mei", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "December"];
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      matchArray: [],
      matches: [],
      selectedMatch: null
    };
    this.handlerNewMatch = this.handlerNewMatch.bind(this)
    this.handlerCloseMatch = this.handlerCloseMatch.bind(this);
  }
  public componentDidMount() {
    this.getMatchesFromApi();
  }
  private handlerNewMatch(match: IMatchInterface) {
    this.setState({
      selectedMatch: match
    })
  }
  private handlerCloseMatch() {
    this.setState({
      selectedMatch: null
    })
  }

  private getMatchesFromApi() {
    fetch('https://bergskeazurefunction.azurewebsites.net/api/GetWedstrijden?code=BMJwhV4CO5qa6IepAFmMe4gwuPRkm2sFa67ibOnajRay2pPqpVmw3w==')
      .then((response) => response.json())
      .then((matches: IMatchInterface[]) => {
        const groupByYearFunction = this.groupByYear("datum");
        var eventsYear = groupByYearFunction(matches);
        var array = [];
        Object.keys(eventsYear).forEach(key => {
          array.push(this.getMatchesFromKey(eventsYear, key));
        });
        this.setState({
          loading: false,
          matchArray: array,
          matches: matches
        });
      })
      .catch((error) => {
        console.error(error);
      });
  }
  
  render() {
    
    return (
      <View>
        
        { 
          this.state.loading 
            ? <ActivityIndicator size="large" color="#0000ff" style={{marginTop: 60}}/>
            : this.state.selectedMatch == null 
              ? 
              <View>
                <Header
                  centerComponent={{ text: "Wedstrijden", style: { color: '#fff', fontSize: 20  }}}
                  leftComponent={{ icon: 'home', color: '#fff', onPress: () => this.changeParentState() }} 
                />
                <SectionList
                ItemSeparatorComponent={this.FlatListItemSeparator}
                sections={this.state.matchArray}
                renderSectionHeader={({ section }) => (
                    <Text style={styles.SectionHeaderStyle}> {section.title} </Text>
                )}
                renderItem={({ item }) => (
                  <CustomListItem match={item.match} handler={this.handlerNewMatch}/>
                )}
                keyExtractor={(item, index) => index.toString()}
              />
              </View>
              : <MatchDetailView selectedMatch={this.state.selectedMatch} closeMatch={this.handlerCloseMatch} parentHandler={this.props.handler}/>
        }
        </View>
    );
  }
  private changeParentState() : void {
    this.props.handler();
  }
  private getMatchesFromKey(eventsYear: any, key: string) : any {
    var obj = {};
    obj["title"] = this.getMonth(parseInt(key.split('-')[1])) + " " + key.split('-')[0];
    var objArray = [];
    eventsYear[key].forEach((match: IMatchInterface) => {
      var dataObj = {};
      dataObj['id'] = match.id;
      dataObj['value'] = match.tegenstanderNaam
      dataObj['match'] = match;
      objArray.push(dataObj);
    });
    obj["data"] = objArray;
    return obj;
  }

  private getMonth(monthNumber: number): string {
    return this.monthNames[monthNumber - 1];
  }

  private GetSectionListItem = (itemId: number) => {
    var match = this.state.matches.filter(x => x.id == itemId)[0];
    console.log("Match with id: " + match.id + " selected");
    this.setState({
      selectedMatch: match
    });
  };
  private FlatListItemSeparator = () => {
    return (
      <View style={{height: 1, width: '100%', backgroundColor: '#C8C8C8'}}/>
    );
  };
  private groupByYear = (key: string) => (array: any) =>
  array.reduce(
    (objectsByKeyValue, obj) => ({
      ...objectsByKeyValue,
      [new Date(obj[key].toString()).getFullYear() + "-" + (new Date(obj[key].toString()).getMonth() + 1)]: (objectsByKeyValue[new Date(obj[key].toString()).getFullYear() + "-" + (new Date(obj[key].toString()).getMonth() + 1)] || []).concat(obj)
    }),
    {}
  )

}

const styles = StyleSheet.create({
    SectionHeaderStyle: {
        backgroundColor: '#CDDC89',
        fontSize: 20,
        padding: 5,
        color: '#fff',
    },
    SectionListItemStyle: {
        fontSize: 15,
        padding: 15,
        color: '#000',
        backgroundColor: '#F5F5F5',
    },
});