import React, { Component } from 'react';
import {
  View,
} from 'react-native';
import { 
  Text,
} from "react-native-elements";
import { IMatchSpelersProps } from '../classes/IMatchSpelersProps';

export default class MatchSpelersComponent extends React.Component<IMatchSpelersProps, {}> {
  constructor(props) {
    super(props);
  }
  public componentDidMount() : void {
    this.getSpelers();
  }

  render() {
    return (
      <View>
        <Text h1>Spelers</Text>
        <Text>
            {this.props.matchId}
        </Text>
      </View>
    );
  }
  private getSpelers() : void {
    // Get spelers van de match hier
  }
}