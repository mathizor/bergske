import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import HomeComponent from './components/HomeComponent';
import { Header } from 'react-native-elements';

export default function App() {
  return (
    <View>
      <HomeComponent />
    </View>
  );
}