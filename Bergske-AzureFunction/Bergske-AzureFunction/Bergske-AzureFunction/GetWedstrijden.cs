using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Data.SqlClient;
using Bergske.Classes;
using System.Collections.Generic;

namespace Bergske_AzureFunction
{
    public static class GetWedstrijden
    {
        [FunctionName("GetWedstrijden")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = null)] HttpRequest req,
            ILogger log)
        {
            List<Game> games = new List<Game>();
            var str = "Server=tcp:bergskesql.database.windows.net,1433;Initial Catalog=Bergske;Persist Security Info=False;User ID=Mathijs;Password=Ge23ra80;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
            using (var conn = new SqlConnection(str))
            {
                conn.Open();
                var text = "select * from dbo.Wedstrijden w INNER JOIN(SELECT Naam as 'TegenstanderNaam', Id as 'TID' FROM dbo.Tegenstanders) l on l.TID = w.FK_Tegenstander_Id order by Datum asc";
                using (var cmd = new SqlCommand(text, conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        AddGame(reader, games);
                    }
                }
            }

            return (ActionResult)new OkObjectResult(games);
        }

        private static void AddGame(SqlDataReader reader, List<Game> games)
        {
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    var t = new Game();

                    for (var inc = 0; inc < reader.FieldCount; inc++)
                    {
                        if (!reader.IsDBNull(inc))
                        {
                            var type =
                                t.GetType();
                            var prop = type.GetProperty(reader.GetName(inc));
                            prop.SetValue(t, Convert.ChangeType(reader.GetValue(inc), prop.PropertyType));
                        }
                    }

                    games.Add(t);
                }
            }
        }
    }
}
