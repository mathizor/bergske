﻿using System;

namespace Bergske.Classes
{
    public class Game
    {
        public  int ID { get; set; }
        public DateTime Datum { get; set; }
        public Opponent Opponent { get; set; }
        public int FK_Tegenstander_Id { get; set; }
        public string TegenstanderNaam { get; set; }
        public int DoelpuntenVoor { get; set; }
        public int DoelpuntenTegen { get; set; }
        public bool Thuis { get; set; }
        public int TID { get; set; }

        public Game()
        {

        }
    }
}
